# a curated list of awesome oberon papers and modules.

# papers

http://norayr.am/papers/fff97.pdf

https://kamee.gitlab.io/tech/notes/an-overview/


# system

http://cas.inf.ethz.ch/projects/a2

# compiler

[voc](https://github.com/vishaps/voc)


# fun

https://www.youtube.com/user/xenopusRTRT/videos

# books

[Modula-2: A Complete Guide (College)](https://b-ok.cc/book/2701214/e515c0) by Kimberly Nelson King

[Programming in Oberon](https://inf.ethz.ch/personal/wirth/Oberon/PIO.pdf) by Niklaus Wirth

[Into the Realm of Oberon: An Introduction to Programming and the Oberon-2 Programming Language](https://b-ok.cc/book/2098185/4afae3) by Eric W. Nikitin

[Programming in Oberon: Steps Beyond Pascal and Modula](https://b-ok.cc/book/679869/611b48) by Martin Reiser, Niklaus Wirth

[A Guide to Modula-2](https://b-ok.cc/book/2116686/64b0ff) by Kaare Christian

[HOW TO PROGRAM A COMPUTER (Using the oo2c Oberon-2 Compiler)](http://www.waltzballs.org/other/prog.html) by Donald Daniel

[Programming in Modula-2](https://b-ok.cc/book/2116360/f0c791)  by Niklaus Wirth

[Object-Oriented Programming: in Oberon-2](https://b-ok.cc/book/2118005/1f5cfa) by  Hanspeter Mössenböck

[Oberon-2 Programming with Windows [With Full Windwos Based Integrated Development]](https://b-ok.cc/book/2098364/b630fb) by  by Jörg Mühlbacher, B. Kirk, U. Kreuzeder


# IDEs, Editors, and Plugins

[Free Oberon](https://freeoberon.su/en/) - A cross-platform IDE for development in Oberon programming language made in the classical FreePascal-like pseudo-graphic style.
